// Routes
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../modules/core/index';


export class GlobalMethods {
  constructor(private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}


  routeToById(pageroute: string, id) {
    this.routerext.navigate([pageroute], {
      queryParams: { id: id },
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
