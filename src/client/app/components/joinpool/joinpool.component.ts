// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { SignUp } from '../../interfaces/user';
import { LoginPool, PoolCreater } from '../../interfaces/pool';

// services
import { AcccountService } from '../../services/account.services';
import { PoolService } from '../../services/pool.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Rx';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'ss-pool',
  templateUrl: 'joinpool.component.html',
  styleUrls: ['../css/bootstrap.css','../css/style1.css'],
  providers: [AcccountService, PoolService]
})

export class JoinPoolComponent implements OnInit {

  user: SignUp; poolId = '';
  userName; password = ''; user_id= '';pageId = 0;

  msgs: Message[] = [];

  constructor(private acccountService: AcccountService,
    private poolService: PoolService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        console.log('+params', +params['id']);
        this.pageId = +params['id'] || 0;
      });

    if (!localStorage.getItem('currentUser') ||
      !localStorage.getItem('token')) {
        this.routeTo('/signin',2);
    }

    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.acccountService.getById(this.user._id).subscribe(user => {
          if(user) {
            console.log('user find : ' , user.userName);
            this.user_id = user._id;
            this.cleraAllfield();
          } else {
            this.routeTo('/signin',2);
          }
        }
        , error => this.logError('User Not Found ' + error, 'error')
      );
    }
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  loginPool() {
    let poolId = this.poolId;
    let password = this.password;
    this.poolService.getBy(poolId).subscribe((pool) => {
        if (pool) {
          if (!pool.isDelete && pool.isActive) {
            console.log('pool found');
            let login : LoginPool = {} as any;
            login.id = poolId; login.password = password;
            login.user_id = this.user_id;
            this.poolService.signIn(login)
              .subscribe((data) => {
                  // console.log(JSON.stringify(data.token));
                  pool.password = password;
                  pool.league_id = pool.league_id;
                  pool.poolType_id = pool.poolType_id;
                  pool.poolEvent_id = pool.poolEvent_id;
                  localStorage.setItem('currentPool', JSON.stringify(pool));
                  let poolCreater : PoolCreater = {} as any;
                  poolCreater.id = login.id;
                  poolCreater.createdBy = this.user_id;
                  console.log('succes' + poolCreater.id + 'bool ' + pool.createdBy._id);
                  this.poolService.getPoolByPC(poolCreater).subscribe(succes => {
                      // let timer = Observable.timer(3000,2000);
                      // timer.subscribe(t=> {
                        console.log('succes', succes.status);
                        if(succes.status) {
                          this.cleraAllfield();
                          this.routeToOn('/dash');
                        } else {
                          this.cleraAllfield();
                          this.routeToOn('/chosen');
                        }
                      // });
                  },
                  err => {
                    this.logError('Pool Not Found ' + err, 'error');
                  });
                },
                (err) => {
                  this.logError('Pool login failed ', 'error');
                });
          } else {
            this.logError('Pool Have Been Deleted By Admin OR Not Active ', 'error');
          }
        } else {
          this.logError('Pool Not Found ', 'error');
        }
      },
      (err) => {
        this.logError('Pool Not Found ' + err, 'error');
      });
  }

  cleraAllfield() {
    this.password = this.poolId = '';
  }

  routeTo(pageroute: string, id) {
    this.routerext.navigate([pageroute], {
      queryParams: { id: id },
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  routeToOn(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
