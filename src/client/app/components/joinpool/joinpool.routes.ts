import { JoinPoolComponent } from './joinpool.component';

export const JoinPoolRoutes: Array<any> = [
  {
    path: 'join',
    component: JoinPoolComponent
  }
];
