import { LoginComponent } from './login.component';

export const LoginRoutes: Array<any> = [
  {
    path: 'signin',
    component: LoginComponent
  }
];
