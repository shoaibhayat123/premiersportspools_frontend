// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { SignUp, SignIn } from '../../interfaces/user';

// services
import { AcccountService } from '../../services/account.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Rx';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'ss-login',
  templateUrl: 'login.component.html',
  styleUrls: ['../css/bootstrap.css','../css/style1.css'],
  providers: [AcccountService]
})

export class LoginComponent implements OnInit {

  user: SignUp;
  userName = ''; password = ''; user_id= '';pageId = 0;

  msgs: Message[] = [];

  // @ViewChild('closeBtn') closeBtn: ElementRef;
  // @ViewChild('closeLoginBtn') closeLoginBtn: ElementRef;

  constructor(private acccountService: AcccountService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        console.log('+params', +params['id']);
        this.pageId = +params['id'] || 0;
      });



    if (!localStorage.getItem('token')) {
      this.signOut();
    } else {
      this.routeTo('/',0);
    }

    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.acccountService.getById(this.user._id).subscribe(user => {
          if(user) {
            console.log('user find : ' , user.userName);
            this.user_id = user._id;
          } else {
            this.signOut();
            // this.routeTo('/signin');
          }
        }
        , error => this.logError('Finding User Error ' + error, 'error')
      );
    }

    this.cleraAllfield();
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  signIn() {
    let email = this.userName;
    let password = this.password;
    this.acccountService.getBy(email.trim()).subscribe((user) => {
      if (user) {
          if (!user.isDelete && user.isActive) {
            console.log('user found');
            let login : SignIn = {} as any;
            login.email = email; login.password = password;
            login.userName = email;
            this.acccountService.signIn(login)
              .subscribe((data) => {
                  // console.log(JSON.stringify(data.token));
                user.password = password;
                  localStorage.setItem('token', data.token);
                  localStorage.setItem('currentUser', JSON.stringify(user));
                  // this.closeLoginBtn.nativeElement.click();
                  this.cleraAllfield();
                  if(this.pageId === 1)
                    this.routeToOn('/pool');
                  else if(this.pageId === 2)
                    this.routeToOn('/join');
                  else
                    this.routeToOn('/');
              },
                (err) => {
                  this.logError('Authentication Failed ', 'error');
                  // this.msgs = [];
                  // this.msgs.push({
                  //   severity: 'error',
                  //   summary: 'Error Message',
                  //   detail:
                  // });
                });
          } else {
            this.logError('User Have Been Deleted By Admin OR Not Active Your Account ', 'error');
            // this.msgs = [];
            // this.msgs.push({
            //   severity: 'info',
            //   summary: 'Info Message',
            //   detail: 'User Have Been Deleted By Admin OR Not Active Your Account'
            // });
          }
      } else {
        this.logError('User Not Found ', 'error');
        // this.msgs = [];
        // this.msgs.push({severity: 'warn', summary: 'Warn Message', detail: 'No User Found'});
      }
    },
      (err) => {
        this.logError('User Not Found ' + err, 'error');
        // this.msgs = [];
        // this.msgs.push({severity: 'warn', summary: 'Warn Message', detail: err});

      });
  }

  signOut() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    localStorage.removeItem('pool');
    localStorage.removeItem('currentPool');
  }

  cleraAllfield() {
    this.password = this.userName = '';
  }

  routeTo(pageroute: string, id) {
    this.routerext.navigate([pageroute], {
      queryParams: { id: id },
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  routeToOn(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
