// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { Pool, Count, PoolMemReq, PoolScore, PoolType } from '../../interfaces/pool';
import { SignUp } from '../../interfaces/user';

// services
import { PoolService } from '../../services/pool.services';
import { PoolEventService } from '../../services/poolevent.services';
import { PoolTypeService } from '../../services/pooltype.services';
import { LeagueService } from '../../services/league.services';
import { AcccountService } from '../../services/account.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'ss-pick',
  templateUrl: 'picks.component.html',
  styleUrls: ['../css/style.css','../css/res.css'],
  providers: [PoolService, PoolEventService, PoolTypeService,
    LeagueService, AcccountService]
})

export class PicksComponent implements OnInit {
  user: SignUp; user_id = ''; userName; poolName; options = ''; sets = '';
  tempPool: Pool; shortName; shrtName = ''; short = false; fullName = '';
  msgs: Message[] = []; item = [];

  constructor(private poolService: PoolService,
              private poolEventService: PoolEventService,
              private poolTypeService: PoolTypeService,
              private leagueService: LeagueService,
              private acccountService: AcccountService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {
    for(let i = 1; i <= 100; i++) {
      this.item[i] = ' ';
    }
  }

  ngOnInit() {
    if (!localStorage.getItem('currentUser') ||
      !localStorage.getItem('token')) {
      this.routeTo('/signin');
    }

    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.acccountService.getById(this.user._id).subscribe(user => {
          if(user) {
            console.log('pick user find : ' , user.userName);
            this.user_id = user._id;
            this.userName = user.userName;
            if(localStorage.getItem('currentPool')) {
              this.tempPool = JSON.parse(localStorage.getItem('currentPool'));
              this.poolName = this.tempPool.name;
              this.fullName = this.user.fullName;
              let poolMemReq: PoolMemReq = {} as any;
              poolMemReq.pool_id = this.tempPool.id;
              poolMemReq.user_id = this.user_id;
              this.poolService.getPoolMemByUP(poolMemReq).subscribe(mem => {
                  if(mem.shortName !== '') {
                    console.log('mem' + mem.shortName);
                    this.shortName = this.shrtName = mem.shortName;
                    this.short = true;
                  }
                  let poolScore: PoolScore = {} as any;
                  poolScore.pool_id = this.tempPool.id;
                  poolScore.quater = '1';
                  this.poolService.getScoreByPQ(poolScore).subscribe(scoreCard => {
                      console.log('scoreArr', scoreCard.scoreArr.length);
                      for(let i = 0; i < scoreCard.scoreArr.length; i++) {
                        let scoreArr = scoreCard.scoreArr[i];
                        console.log('scoreArr', scoreArr);
                        var array = scoreArr.split(',');
                        console.log(array[0] + ',' + array[1] + ',' + array[2]);
                        this.item[array[2]] = array[1];
                      }
                    },
                    err => {
                      this.logError('Error in load board ' + err, 'error');
                    });
                },
                err => {
                  console.log('err', err);
                });
            }
          } else {
            this.routeTo('/signin');
          }
        }
        , error => this.logError('User Not Found ' + error, 'error')
      );
    }
  }

  onSearchChange(val: any) {
    if(this.shrtName === '') {
      console.log('val in update', val);
      let poolMemReq: PoolMemReq = {} as any;
      poolMemReq.pool_id = this.tempPool.id;
      poolMemReq.user_id = this.user_id;
      poolMemReq.shortName = val;
      this.poolService.getPoolMemByUPAndUpdate(poolMemReq).subscribe(mem => {
          console.log('member Update', mem);
          this.shortName = this.shrtName = mem.shortName;

        },
        err => {
          this.logError('Error in set the short name', 'error');
        });
    } else {
      this.shortName = this.shrtName;
      this.logError('Abbrivation Already Set OR You Cannot Change More', 'info');
    }
  }


  getVal(val: any) {
    if(this.shrtName !== '') {
      // console.log('val', val.originalTarget.attributes.index.value);
      console.log('val', val.attributes.index.value);
      let poolScore: PoolScore = {} as any;
      poolScore.pool_id = this.tempPool.id;
      poolScore.user_id = this.user_id;
      poolScore.quater = '1';
      poolScore.index = val.attributes.index.value;
      this.poolService.pickUp(poolScore).subscribe(scoreCard => {
        for(let i = 0; i < scoreCard.scoreArr.length; i++) {
          let scoreArr = scoreCard.scoreArr[i];
          console.log('scoreArr', scoreArr);
          var array = scoreArr.split(',');
          console.log(array[0] + ',' + array[1] + ',' + array[2]);
          this.item[array[2]] = array[1];
        }
        // val.innerHTML = this.shrtName;
      }, err => {
        this.logError('No more picks ' + err, 'error');
      });
    } else {
      this.logError('Please Set Abbrivation', 'info');
    }
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  cleraAllfield() {
    // this.max = '';
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
