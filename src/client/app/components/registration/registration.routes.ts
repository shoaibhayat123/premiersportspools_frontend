import { RegistrationComponent } from './registration.component';

export const RegistrationRoutes: Array<any> = [
  {
    path: 'signup',
    component: RegistrationComponent
  }
];
