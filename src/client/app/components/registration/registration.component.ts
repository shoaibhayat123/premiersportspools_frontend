// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { SignUp } from '../../interfaces/user';

// services
import { AcccountService } from '../../services/account.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Rx';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'ss-signup',
  templateUrl: 'registration.component.html',
  styleUrls: ['../css/bootstrap.css','../css/style1.css'],
  providers: [AcccountService]
})

export class RegistrationComponent implements OnInit {

  user : SignUp;
  users: SignUp[] = [];
  fullName; userName; password; repassword; email; phone; user_id = 'admin';
  subscription;
  msgs: Message[] = [];

  // @ViewChild('closeBtn') closeBtn: ElementRef;
  // @ViewChild('closeLoginBtn') closeLoginBtn: ElementRef;

  constructor(private acccountService: AcccountService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    // localStorage.removeItem('isLogin');
    if (localStorage.getItem('currentUser') ||
      localStorage.getItem('token')) {
      this.routeTo('/dash');
      // if(localStorage.getItem('isLogin') === 'yes') {
      //   console.log('signup page');
      // } else {
      //   this.routeTo('/');
      // }
    }

    // if (localStorage.getItem('currentUser')) {
    //   this.user = JSON.parse(localStorage.getItem('currentUser'));
    //   console.log(this.user._id);
    //   this.acccountService.getById(this.user._id).subscribe(user => {
    //       if(user) {
    //         console.log('user find : ' , user.userName);
    //         this.user_id = user._id;
    //         if(localStorage.getItem('isLogin') === 'yes') {
    //           this.fullName = user.fullName;
    //           this.userName = user.userName;
    //           this.email = user.email;
    //           this.password = this.user.password ;
    //           this.phone = user.phoneNumber;
    //           this.repassword = this.user.password;
    //         }
    //       } else {
    //         localStorage.removeItem('currentUser');
    //         localStorage.removeItem('token');
    //         // this.routeTo('/signin');
    //       }
    //     }
    //     , error => this.logError(error)
    //   );
    // }
  }

  signUp() {
    if(this.password === this.repassword) {
      let user: SignUp = {} as any;
      user.fullName = this.fullName;
      user.userName = this.userName;
      user.email = this.email;
      user.password = this.password;
      user.phoneNumber = this.phone;
      user.isActive = false;
      user.isDelete = false;
      user.isAdmin = false;
      // user.createdBy = this.user_id;
      // if(localStorage.getItem('isLogin') === 'yes') {
      //     this.acccountService.updateUser(this.user_id, user)
      //       .subscribe(
      //         (data) => {
      //           // this.closeBtn.nativeElement.click();
      //           console.log('User Edit : ', JSON.stringify(data));
      //           this.cleraAllfield();
      //           localStorage.removeItem('isLogin');
      //           this.routeTo('/');
      //         },
      //         (err) => {
      //           this.logError(err);
      //         });
      // } else {
        this.acccountService.signUp(user).subscribe(
          (data) => {
            // this.closeBtn.nativeElement.click();
            console.log('User Create : ', JSON.stringify(data));
            this.logError('Successfully SignUp! Confirmation mail send ', 'success');
            let timer = Observable.timer(3000,2000);
            this.subscription = timer.subscribe(t=> {
              console.log('signin timer');
              this.cleraAllfield();
              this.routeTo('/signin');
            });

          },
          (err) => {
            this.logError('Error! On create user or signUp OR User Alerady Exist ', 'error');
          });
      // }
    } else {
      this.logError('Password Not Matching', 'info');
    }

  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  cleraAllfield() {
    this.repassword = this.fullName = this.password = this.userName = this.email = this.phone  = '';
    this.subscription.unsubscribe();
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
