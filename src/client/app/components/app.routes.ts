// app
import { HomeRoutes } from './home/home.routes';
import { AboutRoutes } from './about/about.routes';
import { LoginRoutes } from './login/login.routes';
import { RegistrationRoutes } from './registration/registration.routes';
import { DashboardRoutes } from './dashboard/dashboard.routes';
import { PoolRoutes } from './pool/pool.routes';
import { PoolSettingRoutes } from './poolsetting/setting.routes';
import { PoolDashboardRoutes } from './pooldashboard/pdashboard.routes';
// import { RoasterRoutes } from './roaster/roaster.routes';
// import { InvitesRoutes } from './invites/invites.routes';
import { JoinPoolRoutes } from './joinpool/joinpool.routes';
// import { PicksRoutes } from './picks/picks.routes';
import { ChosenRoutes } from './chosen/chosen.routes';
import { ChosenDashboardRoutes} from './chosen/chosenDashboard.routes';
import { PoolDashboardClientRoutes } from './pooldashboard/pooldashboard.routes';

export const routes: Array<any> = [
  ...HomeRoutes,
  ...AboutRoutes,
  ...LoginRoutes,
  ...RegistrationRoutes,
  ...DashboardRoutes,
  ...PoolRoutes,
  ...PoolSettingRoutes,
  ...PoolDashboardRoutes,
  // ...RoasterRoutes,
  // ...InvitesRoutes,
  ...JoinPoolRoutes,
  // ...PicksRoutes,
  ...ChosenRoutes,
  ...PoolDashboardClientRoutes,
  ...ChosenDashboardRoutes
];
