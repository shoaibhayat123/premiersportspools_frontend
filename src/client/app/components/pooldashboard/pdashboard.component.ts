// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';
import { Location } from '@angular/common';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { Pool, Count, PoolLeague, PoolEvent, PoolType } from '../../interfaces/pool';
import { SignUp } from '../../interfaces/user';

// services
import { PoolService } from '../../services/pool.services';
import { PoolEventService } from '../../services/poolevent.services';
import { PoolTypeService } from '../../services/pooltype.services';
import { LeagueService } from '../../services/league.services';
import { AcccountService } from '../../services/account.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Rx';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'ss-pool',
  templateUrl: 'pdashboard.component.html',
  styleUrls: ['../css/style.css','../css/res.css'],
  providers: [PoolService, PoolEventService, PoolTypeService,
    LeagueService, AcccountService]
})

export class PoolDashboardComponent implements OnInit {

  max; leagueName = ''; typeName = '';
  user: SignUp; user_id = ''; userName; poolName; options = ''; sets = '';
  tempPool: Pool; joinPools; createPools;
  msgs: Message[] = []; dTab = true; iTab = false; pTab = false;
  rTab = false;

  // @ViewChild('closeBtn') closeBtn: ElementRef;
  // @ViewChild('closeLoginBtn') closeLoginBtn: ElementRef;

  constructor(private poolService: PoolService,
              private poolEventService: PoolEventService,
              private poolTypeService: PoolTypeService,
              private leagueService: LeagueService,
              private location: Location,
              private acccountService: AcccountService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    var islst = true;
    localStorage.removeItem('pool');
    if (!localStorage.getItem('currentUser') ||
      !localStorage.getItem('token')) {
      this.routeTo('/signin');
    }

    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.acccountService.getById(this.user._id).subscribe(user => {
          if(user) {
            this.userName = user.userName;
            console.log('user find & Id: ' + user.userName + ' & ' + user._id);
            this.poolService.getByUserId(user._id).subscribe(pools => {
              if(pools.joinPools) {
                this.joinPools = pools.joinPools;
                console.log('this.joinPools', this.joinPools);
              }
              if(pools.createPools) {
                this.createPools = pools.createPools;
                console.log('this.createPools', this.createPools);
              }
            }, err => {
              this.logError('Error! Getting Pools ' + err, 'error');
            });

            if(localStorage.getItem('currentPool')) {
              var pool = JSON.parse(localStorage.getItem('currentPool'));
              console.log('pool Id' , pool.id);
              this.poolService.getBy(pool.id).subscribe(data => {
                if(data) {
                  console.log('find data poll' , data);
                  this.fillform(pool);
                  islst = true;
                }
              },
              err => {
                this.logError('Error! Pool Not Found ' + err, 'error');
              });
            }
          } else {
            this.routeTo('/signin');
          }
        }
        , error => this.logError('User Not Found ' + error, 'error')
      );
    }

    if(!islst) {
      console.log(islst);
      this.routeTo('/signin');
      // this.poolService.getLastPoool(this.user._id).subscribe(lstPool => {
      //     this.count = lstPool;
      //     this.poolId = this.count.id;
      //     console.log('this.poolId', this.poolId);
      //   },
      //   error => this.logError(error)
      // );
    }
  }

  goToPool(id: any) {
    console.log('pool id', id);
    this.poolService.getById(id).subscribe(pool => {
        // console.log('pool pool id', pool.poolType_id._id);
        pool.league_id = pool.league_id === null ? ' ': pool.league_id._id;
        pool.poolType_id = pool.poolType_id === null ? ' ': pool.poolType_id._id;
        pool.poolEvent_id = pool.poolEvent_id === null ? ' ': pool.poolEvent_id._id;
        localStorage.setItem('currentPool', JSON.stringify(pool));
        // this.fillform(pool);
        // this.routeTo('/dash');
        this.logError('Loading Pool ' + pool.id + ' Dashboard.... ', 'succes');
        let timer = Observable.timer(2000,1000);
          timer.subscribe(t=> {
            console.log('loading...');
            location.reload();
        });

        // this.routeTo('/dash');
        // ChangeDetectorRef.markForCheck();
    },
    err => {
      this.logError('Error! Move on current pool ' + err, 'error');
    });
  }

  lastPool() {
    if (localStorage.getItem('currentUser')) {
      let user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(user._id);
      this.poolService.getLastPool(user._id).subscribe(pool => {
          pool.league_id = pool.league_id === null ? ' ': pool.league_id._id;
          pool.poolType_id = pool.poolType_id === null ? ' ': pool.poolType_id._id;
          pool.poolEvent_id = pool.poolEvent_id === null ? ' ': pool.poolEvent_id._id;
          localStorage.setItem('currentPool', JSON.stringify(pool));
          this.fillform(pool);
        },
        err => {
          this.logError('Error! Move on last pool ' + err, 'error');
        });
    }
  }

  fillform(pool: Pool) {
    console.log(pool);
    this.poolName = pool.name;

    // Pool Type
    this.poolTypeService.getById(pool.poolType_id).subscribe(type => {
        this.typeName = type.name;
      }
      , error => this.logError('Error! Get Pool Type ' + error, 'error')
    );

    // League
    this.leagueService.getById(pool.league_id).subscribe(leagues => {
        this.leagueName = leagues.name;
      }
      , error => this.logError('Error! Get Sport OR League ' + error, 'error')
    );
    this.options = pool.winnerFreq;
    this.sets = pool.numSets;
    this.max = pool.maxEntries;
  }

  tagChange(tab: string) {
    if(tab === 'R') {
      this.dTab = false;
      this.rTab = true;
      this.iTab = false;
      this.pTab = false;
    }

    if(tab === 'I') {
      this.dTab = false;
      this.rTab = false;
      this.iTab = true;
      this.pTab = false;
    }

    if(tab === 'P') {
      this.dTab = false;
      this.rTab = false;
      this.iTab = false;
      this.pTab = true;
    }

    if(tab === 'D') {
      this.dTab = true;
      this.rTab = false;
      this.iTab = false;
      this.pTab = false;
    }
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  cleraAllfield() {
    // this.repassword = this.password = this.poolName =
    //   this.poolId = this.selectedeventId = this.selectedtypeId =
    //     this.selectedleugueId = '';
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  account() {
    localStorage.setItem('isLogin', 'yes');
    this.routeTo('/signup');
  }
}
