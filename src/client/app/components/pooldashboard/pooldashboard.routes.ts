import { PoolDashboardClientComponent } from './pooldashboard.component';

export const PoolDashboardClientRoutes: Array<any> = [
  {
    path: 'dash',
    component: PoolDashboardClientComponent
  }
];
