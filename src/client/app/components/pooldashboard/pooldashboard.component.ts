// core libs
import { Component, OnInit , ViewEncapsulation, ElementRef, ViewChild} from '@angular/core';
import { Location } from '@angular/common';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { Pool, UserInvites, PoolMemReq, PoolScore, PoolLeague, PoolEvent, PoolType } from '../../interfaces/pool';
import { SignUp } from '../../interfaces/user';

// services
import { PoolService } from '../../services/pool.services';
import { PoolEventService } from '../../services/poolevent.services';
import { PoolTypeService } from '../../services/pooltype.services';
import { LeagueService } from '../../services/league.services';
import { AcccountService } from '../../services/account.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Rx';

// import '../../../node_modules/jquery/dist/jquery.js';
// import '../../../node_modules/bootstrap/dist/js/bootstrap.min.js';

// import '../../../src/client/assets/assets/js/jquery-3.3.1.min.js';
// import '../../../src/client/assets/assets/js/bootstrap.min.js';


declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'ss-pooled',
  templateUrl: 'pooldashboard.component.html',
  styleUrls: ['../css/bootstrap.css','../css/style1.css'],
  providers: [PoolService, PoolEventService, PoolTypeService,
    LeagueService, AcccountService],
  encapsulation: ViewEncapsulation.None
})

export class PoolDashboardClientComponent implements OnInit {

  max; leagueName = ''; typeName = '';
  user: SignUp; user_id = ''; userName; poolName; options = ''; sets = '';
  tempPool: Pool; joinPools; createPools;
  msgs: Message[] = []; dTab = true; iTab = false; pTab = false;
  rTab = false;
  user1 = '';
  user2 = '';
  user3 = '';
  user4 = '';
  user5 = '';
  user6 = '';
  user7 = '';
  user8 = '';
  user9 = '';
  user10 = '';
  user11 = '';
  user12 = '';
  user13 = '';
  user14 = '';
  user15 = '';
  user16 = '';
  user17 = '';
  user18 = '';
  userEmails: any[] = [];
  url = 'http://localhost:5555/join?id=2';
  cols: any[] = [];
  shortName; shrtName = ''; short = false; item = []; displayName = '';
  subscribe; pool_id;
  // @ViewChild('closeBtn') closeBtn: ElementRef;
  // @ViewChild('closeLoginBtn') closeLoginBtn: ElementRef;

  constructor(private poolService: PoolService,
              private poolEventService: PoolEventService,
              private poolTypeService: PoolTypeService,
              private leagueService: LeagueService,
              private location: Location,
              private acccountService: AcccountService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {
    for(let i = 1; i <= 100; i++) {
      this.item[i] = ' ';
    }
  }

  ngOnInit() {
    var islst = true;
    localStorage.removeItem('pool');
    if (!localStorage.getItem('currentUser') ||
      !localStorage.getItem('token')) {
      this.routeTo('/signin');
    }

    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.acccountService.getById(this.user._id).subscribe(user => {
          if(user) {
            this.user_id = user._id;
            this.userName = user.userName;
            console.log('user find & Id: ' + user.userName + ' & ' + user._id);


            if(localStorage.getItem('currentPool')) {
              this.tempPool = JSON.parse(localStorage.getItem('currentPool'));
              console.log('pool Id' , this.tempPool.id);
              this.poolService.getBy(this.tempPool.id).subscribe(data => {
                  if (data) {
                    this.pool_id = data._id;
                    console.log('find data poll', data);
                    this.fillform(this.tempPool);
                    islst = true;
                this.poolService.getByUserId(user._id).subscribe(pools => {
                  if (pools.joinPools) {
                    this.joinPools = pools.joinPools;
                    console.log('this.joinPools', this.joinPools);
                  }
                  if (pools.createPools) {
                    this.createPools = pools.createPools;
                    console.log('this.createPools', this.createPools);
                  }
                }, err => {
                  this.logError('Error! Getting Pools ' + err, 'error');
                });

                // roaster data
                this.poolService.getPoolMemBy(this.tempPool.id).subscribe(succes => {
                    console.log('succes', succes.PoolMember);
                    for (let i = 0; i < succes.PoolMember.length; i++) {
                      let pooldata = JSON.stringify(succes.PoolMember[i]);
                      console.log('pooldata', pooldata);

                      this.cols.push({data: succes.PoolMember[i], poolEntry: this.tempPool.maxEntries});
                    }
                  },
                  err => {
                    this.logError('Error! On loading pool members ' + err, 'error');
                  });

                // pick data
                let poolMemReq: PoolMemReq = {} as any;
                poolMemReq.pool_id = this.tempPool.id;
                poolMemReq.user_id = this.user_id;
                this.poolService.getPoolMemByUP(poolMemReq).subscribe(mem => {
                    if (mem.shortName !== '') {
                      console.log('mem' + mem.shortName);
                      this.shortName = this.shrtName = mem.shortName;
                      this.short = true;
                    }
                    this.displayName = mem.displayName;
                    let poolScore: PoolScore = {} as any;
                    poolScore.pool_id = this.tempPool.id;
                    poolScore.quater = '1';
                    this.poolService.getScoreByPQ(poolScore).subscribe(scoreCard => {
                        console.log('scoreArr', scoreCard.scoreArr.length);
                        for (let i = 0; i < scoreCard.scoreArr.length; i++) {
                          let scoreArr = scoreCard.scoreArr[i];
                          console.log('scoreArr', scoreArr);
                          var array = scoreArr.split(',');
                          console.log(array[0] + ',' + array[1] + ',' + array[2]);
                          this.item[array[2]] = array[1];
                        }
                      },
                      err => {
                        this.logError('Error in load board ' + err, 'error');
                      });
                  },
                  err => {
                    console.log('err', err);
                  });
              } else {
                this.lastPool();
              }
            });
            } else {
              this.lastPool();
            }
          } else {
            this.routeTo('/signin');
          }
        }
        , error => this.logError('User Not Found ' + error, 'error')
      );
    }

    if(!islst) {
      console.log(islst);
      this.routeTo('/signin');
      // this.poolService.getLastPoool(this.user._id).subscribe(lstPool => {
      //     this.count = lstPool;
      //     this.poolId = this.count.id;
      //     console.log('this.poolId', this.poolId);
      //   },
      //   error => this.logError(error)
      // );
    }
  }

  goToPool(id: any) {
    console.log('pool id' , id);
    if(this.pool_id !== id) {
      this.poolService.getById(id).subscribe(pool => {
          // console.log('pool pool id', pool.poolType_id._id);
          pool.league_id = pool.league_id === null ? ' ' : pool.league_id;
          pool.poolType_id = pool.poolType_id === null ? ' ' : pool.poolType_id;
          pool.poolEvent_id = pool.poolEvent_id === null ? ' ' : pool.poolEvent_id;
          localStorage.setItem('currentPool', JSON.stringify(pool));
          // this.fillform(pool);
          // this.routeTo('/dash');
          // this.logError('Loading Pool ' + pool.id + ' Dashboard.... ', 'succes');
          // let timer = Observable.timer(2000,1000);
          // timer.subscribe(t=> {
          //   console.log('loading...');
          //   location.reload();
          // });
          console.log('loading...');
          location.reload();
          // this.routeTo('/dash');
          // ChangeDetectorRef.markForCheck();
        },
        err => {
          this.logError('Error! Move on current pool ' + err, 'error');
        });
    } else {
      this.logError('You are already on this pool ', 'info');
    }
  }

  lastPool() {
    if (localStorage.getItem('currentUser')) {
      let user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(user._id);
      this.poolService.getLastPool(user._id).subscribe(pool => {
        // if(pool) {
          pool.league_id = pool.league_id === null ? ' ': pool.league_id;
          pool.poolType_id = pool.poolType_id === null ? ' ': pool.poolType_id;
          pool.poolEvent_id = pool.poolEvent_id === null ? ' ': pool.poolEvent_id;
          localStorage.setItem('currentPool', JSON.stringify(pool));
          this.fillform(pool);
          this.routeTo('/dash');
        // } else {
        //   this.logError('You donot have any own created pool, You can join another pool or create you own pool ', 'succes');
        //   let timer = Observable.timer(2000, 1000);
        //   this.subscribe = timer.subscribe(t => {
        //     console.log('loading...');
        //     this.noPool();
        //   });
        //
        // }
        },
        err => {
          this.logError('Error! Move on last pool ' + err, 'error');

          // let timer = Observable.timer(2000, 1000);
          // this.subscribe = timer.subscribe(t => {
          //   console.log('loading...');
          //   this.noPool();
          // });
          this.routeTo('/');
        });
    }
  }

  noPool() {
    this.subscribe.unsubscribe();
    this.routeTo('/');
  }

  fillform(pool: Pool) {
    console.log(pool);
    this.poolName = pool.name;

    // // Pool Type
    // this.poolTypeService.getById(pool.poolType_id).subscribe(type => {
    //     this.typeName = type.name;
    //   }
    //   , error => this.logError('Error! Get Pool Type ' + error, 'error')
    // );
    //
    // // League
    // this.leagueService.getById(pool.league_id).subscribe(leagues => {
    //     this.leagueName = leagues.name;
    //   }
    //   , error => this.logError('Error! Get Sport OR League ' + error, 'error')
    // );
    this.typeName = pool.poolType_id;
    this.leagueName = pool.league_id;
    this.options = pool.winnerFreq;
    this.sets = pool.numSets;
    this.max = pool.maxEntries;
  }

  tagChange(tab: string) {
    if(tab === 'R') {
      this.dTab = false;
      this.rTab = true;
      this.iTab = false;
      this.pTab = false;
    }

    if(tab === 'I') {
      this.dTab = false;
      this.rTab = false;
      this.iTab = true;
      this.pTab = false;
    }

    if(tab === 'P') {
      this.dTab = false;
      this.rTab = false;
      this.iTab = false;
      this.pTab = true;
    }

    if(tab === 'D') {
      this.dTab = true;
      this.rTab = false;
      this.iTab = false;
      this.pTab = false;
    }
  }

  poolSetting() {
    console.log('setting');
  }

  // picks
  onSearchChange(val: any) {
    console.log('val in update', val);
    if(this.shrtName === '' || this.shrtName === null) {
      console.log('val in update', val);
      let poolMemReq: PoolMemReq = {} as any;
      poolMemReq.pool_id = this.tempPool.id;
      poolMemReq.user_id = this.user_id;
      poolMemReq.shortName = val;
      this.poolService.getPoolMemByUPAndUpdate(poolMemReq).subscribe(mem => {
          console.log('member Update', mem);
          this.shortName = this.shrtName = mem.shortName;

        },
        err => {
          this.logError('Error in set the short name', 'error');
        });
    } else {
      this.shortName = this.shrtName;
      this.logError('Abbrivation Already Set OR You Cannot Change More', 'info');
    }
  }

  onDisSearchChange(val: any) {
    // if(this.shrtName === '') {
      console.log('val in update', val);
      let poolMemReq: PoolMemReq = {} as any;
      poolMemReq.pool_id = this.tempPool.id;
      poolMemReq.user_id = this.user_id;
      poolMemReq.displayName = val;
      this.poolService.getPoolMemByUPDisAndUpdate(poolMemReq).subscribe(mem => {
          console.log('member Update', mem);
          this.displayName = mem.displayName;
          // roaster data
          this.cols = [];
          this.poolService.getPoolMemBy(this.tempPool.id).subscribe(succes => {
              console.log('succes', succes.PoolMember);
              for (let i = 0; i < succes.PoolMember.length; i++) {
                let pooldata = JSON.stringify(succes.PoolMember[i]);
                console.log('pooldata', pooldata);

                this.cols.push({data: succes.PoolMember[i], poolEntry: this.tempPool.maxEntries});
              }
            },
            err => {
              this.logError('Error! On loading pool members ' + err, 'error');
            });

        },
        err => {
          this.logError('Error in set the short name', 'error');
        });
    // } else {
    //   this.shortName = this.shrtName;
    //   this.logError('Abbrivation Already Set OR You Cannot Change More', 'info');
    // }
  }


  getVal(val: any) {
    if(this.shrtName !== '') {
      // console.log('val', val.originalTarget.attributes.index.value);
      console.log('val', val.attributes.index.value);
      let poolScore: PoolScore = {} as any;
      poolScore.pool_id = this.tempPool.id;
      poolScore.user_id = this.user_id;
      poolScore.quater = '1';
      poolScore.index = val.attributes.index.value;
      this.poolService.pickUp(poolScore).subscribe(scoreCard => {
        for(let i = 0; i < scoreCard.scoreArr.length; i++) {
          let scoreArr = scoreCard.scoreArr[i];
          console.log('scoreArr', scoreArr);
          var array = scoreArr.split(',');
          console.log(array[0] + ',' + array[1] + ',' + array[2]);
          this.item[array[2]] = array[1];
        }
        // val.innerHTML = this.shrtName;
      }, err => {
        this.logError('No more picks ' + err, 'error');
      });
    } else {
      this.logError('Please Set Abbrivation', 'info');
    }
  }

  // invites tab
  invitesUser() {
    // console.log(this.inputs); //this will give access to id and values (explore it on google chromes console)
    // console.log(this.inputs.toArray().map(x => x.value)) // this gives access to values

    // console.log('count', this.count);
    // for(let i = 1 ; i <= this.count ; this.count++) {
    //   if(this.us)
    // }

    this.userEmails = [];
    if (this.user1) {
      this.userEmails.push(this.user1);
    }
    if (this.user2) {
      this.userEmails.push(this.user2);
    }
    if (this.user3) {
      this.userEmails.push(this.user3);
    }
    if (this.user4) {
      this.userEmails.push(this.user4);
    }
    if (this.user5) {
      this.userEmails.push(this.user5);
    }
    if (this.user6) {
      this.userEmails.push(this.user6);
    }
    if (this.user7) {
      this.userEmails.push(this.user7);
    }
    if (this.user8) {
      this.userEmails.push(this.user8);
    }
    if (this.user9) {
      this.userEmails.push(this.user9);
    }
    if (this.user10) {
      this.userEmails.push(this.user10);
    }
    if (this.user11) {
      this.userEmails.push(this.user11);
    }
    if (this.user12) {
      this.userEmails.push(this.user12);
    }
    if (this.user13) {
      this.userEmails.push(this.user13);
    }
    if (this.user14) {
      this.userEmails.push(this.user14);
    }
    if (this.user15) {
      this.userEmails.push(this.user15);
    }
    if (this.user16) {
      this.userEmails.push(this.user16);
    }
    if (this.user17) {
      this.userEmails.push(this.user17);
    }
    if (this.user18) {
      this.userEmails.push(this.user18);
    }

    let userInvites: UserInvites = {} as any;
    console.log('this.userEmails', this.userEmails);
    userInvites.users = this.userEmails;
    userInvites.route = this.url;
    userInvites.poolId = this.tempPool.id;
    userInvites.password = this.tempPool.password;
    this.poolService.invites(userInvites).subscribe(succes => {
        this.logError('Succesfully invitaions send ', 'succes');
        this.cleraAllfield();
      },
      err => {
        this.logError('Error on invites users ', 'error');
      });
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  cleraAllfield() {
    this.user1 = this.user2 = this.user3 = this.user4 =
      this.user5 = this.user6 = this.user7 = this.user8 =
        this.user9 = this.user10 = this.user11 = this.user12 =
          this.user13 = this.user14 = this.user15 =  this.user16 =
            this.user17 = this.user18 = '';
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  account() {
    localStorage.setItem('isLogin', 'yes');
    this.routeTo('/signup');
  }
}
