import { PoolDashboardComponent } from './pdashboard.component';

export const PoolDashboardRoutes: Array<any> = [
  {
    path: 'cdash',
    component: PoolDashboardComponent
  }
];
