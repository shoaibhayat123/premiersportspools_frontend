import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PoolComponent } from './pool/pool.component';
import { PoolSettingComponent } from './poolsetting/setting.component';
import { PoolDashboardComponent } from './pooldashboard/pdashboard.component';
import { RoasterComponent } from './roaster/roaster.component';
import { JoinPoolComponent } from './joinpool/joinpool.component';
import { InvitesComponent } from './invites/invites.component';
import { PicksComponent } from './picks/picks.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { RightSideComponent } from './rightside/rightside.component';
import { ChosenComponent } from './chosen/chosen.component';
import { ChosenDashboardComponent } from './chosen/chosenDashboard.component';
import { PoolDashboardClientComponent } from './pooldashboard/pooldashboard.component';

export const APP_COMPONENTS: any[] = [
  AppComponent,
  AboutComponent,
  HomeComponent,
  LoginComponent,
  RegistrationComponent,
  DashboardComponent,
  PoolComponent,
  PoolSettingComponent,
  PoolDashboardComponent,
  RoasterComponent,
  JoinPoolComponent,
  InvitesComponent,
  PicksComponent,
  FooterComponent,
  HeaderComponent,
  RightSideComponent,
  ChosenComponent,
  PoolDashboardClientComponent,
  ChosenDashboardComponent
];

export * from './app.component';
export * from './about/about.component';
export * from './home/home.component';
export * from './login/login.component';
export * from './registration/registration.component';
export * from './dashboard/dashboard.component';
export * from  './pool/pool.component';
export * from './poolsetting/setting.component';
export * from './pooldashboard/pdashboard.component';
export * from './roaster/roaster.component';
export * from './joinpool/joinpool.component';
export * from './invites/invites.component';
export * from './picks/picks.component';
export * from './footer/footer.component';
export * from './header/header.component';
export * from './rightside/rightside.component';
export * from './chosen/chosen.component';
export * from './pooldashboard/pooldashboard.component';
export * from './chosen/chosenDashboard.component';
