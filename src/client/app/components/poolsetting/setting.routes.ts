import { PoolSettingComponent } from './setting.component';

export const PoolSettingRoutes: Array<any> = [
  {
    path: 'setting',
    component: PoolSettingComponent
  }
];
