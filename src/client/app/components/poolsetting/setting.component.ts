// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { Pool, Count, PoolLeague, PoolEvent, PoolType } from '../../interfaces/pool';
import { SignUp } from '../../interfaces/user';

// services
import { PoolService } from '../../services/pool.services';
import { PoolEventService } from '../../services/poolevent.services';
import { PoolTypeService } from '../../services/pooltype.services';
import { LeagueService } from '../../services/league.services';
import { AcccountService } from '../../services/account.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'ss-setting',
  templateUrl: 'setting.component.html',
  styleUrls: ['../css/bootstrap.css','../css/style1.css'],
  providers: [PoolService, PoolEventService, PoolTypeService,
    LeagueService, AcccountService]
})

export class PoolSettingComponent implements OnInit {
  title; max; basket = false;
  user: SignUp; user_id = ''; userName; poolName; options = ''; sets = '';
  tempPool: Pool;
  msgs: Message[] = [];

  constructor(private poolService: PoolService,
              private poolEventService: PoolEventService,
              private poolTypeService: PoolTypeService,
              private leagueService: LeagueService,
              private acccountService: AcccountService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    this.title = 'SignUp Page';
    if (!localStorage.getItem('currentUser') ||
      !localStorage.getItem('token')) {
      this.routeTo('/signin');
    }

    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.acccountService.getById(this.user._id).subscribe(user => {
          if(user) {
            this.cleraAllfield();
            console.log('user find : ' , user.userName);
            this.user_id = user._id;
            this.userName = user.userName;
          } else {
            this.routeTo('/signin');
          }
        }
        , error => this.logError('User Not Found ' + error, 'error')
      );
    }

    if(localStorage.getItem('pool')) {
      this.tempPool = JSON.parse(localStorage.getItem('pool'));
      this.poolName = this.tempPool.name;
      this.options = '1';
      this.sets = '1';
      this.basket = true;
      // this.leagueService.getById(this.tempPool.league_id).subscribe(league => {
      //   console.log(' no baskek ball', league.name);
      //   if(league.name.indexOf('Basket') !== -1) {
      //     console.log('baskek ball', league.name);
      //     this.options = '1';
      //     this.basket = true;
      //   }
      // });
    }
  }

  poolCreate() {
    if(this.options === '') {
      console.log(this.options);
      this.logError('Please select winner frequency', 'info');
      return;
    }
    if(this.sets === '') {
      console.log(this.sets);
      this.logError('Please select set', 'info');
      return;
    } else {
      this.tempPool.maxEntries = this.max;
      this.tempPool.numSets = this.sets;
      this.tempPool.winnerFreq = this.options;
      let pool: Pool = {} as any;
      pool = this.tempPool;
      console.log('pool', pool);
      this.poolService.submitPool(pool).map(res => res).subscribe(
        (data) => {
          this.cleraAllfield();
          localStorage.removeItem('pool');
          localStorage.setItem('currentPool', JSON.stringify(pool));
          this.routeTo('/dash');
        },
        (err) => {
          console.log('err', err);
          this.logError('Error! On create pool ' + err, 'error');
        });
    }

  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  cleraAllfield() {
   this.max = '';
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
