import { DashboardComponent } from './dashboard.component';

export const DashboardRoutes: Array<any> = [
  {
    path: '',
    component: DashboardComponent
  }
];
