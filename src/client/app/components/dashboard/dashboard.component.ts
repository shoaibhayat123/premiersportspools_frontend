// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { SignUp, SignIn } from '../../interfaces/user';

// services
import { PoolService } from '../../services/pool.services';
import { AcccountService } from '../../services/account.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Rx';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'ss-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['../css/bootstrap.css','../css/style1.css'],
  providers: [AcccountService, PoolService]
})

export class DashboardComponent implements OnInit {

  isLogin;subscribe;

  msgs: Message[] = [];

  // @ViewChild('closeBtn') closeBtn: ElementRef;
  // @ViewChild('closeLoginBtn') closeLoginBtn: ElementRef;

  constructor(private poolService: PoolService,
    private acccountService: AcccountService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    localStorage.removeItem('pool');
    if (localStorage.getItem('currentUser') ||
      localStorage.getItem('token')) {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }

    if (localStorage.getItem('currentUser')) {
      let user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(user._id);
      this.acccountService.getById(user._id).subscribe(user => {
          if(user) {
            console.log('user find : ' , user.userName);
            // this.user_id = user._id;
          } else {
            this.signOut();
            location.reload();
            // this.routeTo('/signin');
          }
        }
        , error => this.logError('Finding User Error ' + error, 'error')
      );
    }

    // // Users
    // this.acccountService.getUsers().subscribe(users => {
    //     this.users = users;
    //     for (let i = 0; i < this.users.length; i++) {
    //       console.log('users : ', this.users);
    //     }
    //   }
    //   , error => this.logError(error)
    // );
  }

  lastPool() {
    if (localStorage.getItem('currentUser')) {
      let user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(user._id);
      this.poolService.getLastPool(user._id).subscribe(pool => {
            pool.league_id = pool.league_id === null ? ' ': pool.league_id;
            pool.poolType_id = pool.poolType_id === null ? ' ': pool.poolType_id;
            pool.poolEvent_id = pool.poolEvent_id === null ? ' ': pool.poolEvent_id;
            localStorage.setItem('currentPool', JSON.stringify(pool));
            this.routeToOn('/dash');
        },
        err => {
          this.logError('You donot have any own created pool, You can join another pool or create you own pool  ', 'info');
        });
    }
  }

  noPool() {
    this.subscribe.unsubscribe();
    this.routeToOn('/');
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }


  signOut() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    localStorage.removeItem('pool');
    localStorage.removeItem('isLogin');
    this.routeToOn('/signin');
  }

  // cleraAllfield() {
  //   this.firstName = this.password = this.lastName = this.createdBy = this.lat
  //     = this.long = this.userName = this.email = this.age = this.lemail = this.lpassword = '';
  // }

  routeTo(pageroute: string, id) {
    this.routerext.navigate([pageroute], {
      queryParams: { id: id },
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  routeToOn(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
