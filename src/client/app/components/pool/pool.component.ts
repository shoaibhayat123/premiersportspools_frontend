// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { Pool, Count, PoolLeague, PoolEvent, PoolType } from '../../interfaces/pool';
import { SignUp } from '../../interfaces/user';

// services
import { PoolService } from '../../services/pool.services';
import { PoolEventService } from '../../services/poolevent.services';
import { PoolTypeService } from '../../services/pooltype.services';
import { LeagueService } from '../../services/league.services';
import { AcccountService } from '../../services/account.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'ss-pool',
  templateUrl: 'pool.component.html',
  styleUrls: ['../css/bootstrap.css','../css/style1.css'],
  providers: [PoolService, PoolEventService, PoolTypeService,
    LeagueService, AcccountService]
})

export class PoolComponent implements OnInit {

  title; userName; count: Count;
  poolId; poolName; password; repassword; createdBy;
  user: SignUp;
  msgs: Message[] = [];
  tempPool : Pool;
  events : PoolEvent[] = [];
  selectedeventId: string = '';
  types : PoolType[] = [];
  selectedtypeId: string = '';
  leagues : PoolLeague[] = [];
  selectedleugueId: string = '';
  pageId = 0;

  // @ViewChild('closeBtn') closeBtn: ElementRef;
  // @ViewChild('closeLoginBtn') closeLoginBtn: ElementRef;

  constructor(private poolService: PoolService,
              private poolEventService: PoolEventService,
              private poolTypeService: PoolTypeService,
              private leagueService: LeagueService,
    private acccountService: AcccountService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.pageId = +params['id'] || 0;
      });

    this.title = 'SignUp Page';
    if (!localStorage.getItem('currentUser') ||
      !localStorage.getItem('token')) {
      this.routeTo('/signin', this.pageId);
    }

    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.acccountService.getById(this.user._id).subscribe(user => {
            if(user) {
              // this.cleraAllfield();
              console.log('user find : ' , user.userName);
              this.userName = user.userName;
              this.poolService.getCount().subscribe(count => {
                this.count = count;
                this.poolId = this.count.id;
                console.log('this.poolId', this.poolId);
                },
                error => this.logError('Error in generating Pool ID ' + error, 'error')
              );
            } else {
              this.routeTo('/signin',this.pageId);
            }
          }
          , error => this.logError('User Not Found ' + error, 'error')
        );
    }

    // // Pool Event Dropdown
    // this.poolEventService.getPoolEvents().subscribe(events => {
    //     this.events = events;
    //   }
    //   , error => this.logError('Error! Get Pool Event ' + error, 'error')
    // );
    //
    // // Pool Event Dropdown
    // this.poolTypeService.getPoolTypes().subscribe(types => {
    //     this.types = types;
    //   }
    //   , error => this.logError('Error! Get Pool Types ' + error, 'error')
    // );
    //
    // // Pool Event Dropdown
    // this.leagueService.getLeagues().subscribe(leagues => {
    //     this.leagues = leagues;
    //   }
    //   , error => this.logError('Error! Get Sport OR League ' + error, 'error')
    // );

    if(localStorage.getItem('pool')) {
      this.tempPool = JSON.parse(localStorage.getItem('pool'));
      console.log('tempPool', this.tempPool);
      this.poolName = this.tempPool.name;
      this.password = this.repassword = this.tempPool.password;
    }
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  onPoolEventChange(eventId) {
      console.log('eventId', eventId);
      this.selectedeventId = eventId;
  }

  onPoolTypeChange(typeId) {
      console.log('typeId', typeId);
      this.selectedtypeId = typeId;
  }

  onLeagueChange(leagueId) {
      console.log('leagueId', leagueId);
      this.selectedleugueId = leagueId;
  }

  poolCreate() {
    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.createdBy = this.user._id;
    }
    if(this.password === this.repassword) {
      if(this.selectedleugueId === '') {
        this.logError('Please Select League Name', 'info');
        return;
      }
      if(this.selectedtypeId === '') {
        this.logError('Please Select Pool Type Name', 'info');
        return;
      }
      // if(this.selectedeventId === '') {
      //   this.logError('Please Select Pool Event Name');
      //   return;
      // }
      let pool: Pool = {} as any;
      pool.id = this.poolId;
      pool.name = this.poolName;
      pool.league_id = this.selectedleugueId;
      pool.poolType_id = this.selectedtypeId;
      pool.password = this.password;
      pool.poolEvent_id = this.selectedeventId;
      pool.createdBy = this.createdBy;
      localStorage.setItem('pool', JSON.stringify(pool));
      this.cleraAllfield();
      this.routeTo('/setting', 0);
      // this.poolService.submitPool(pool).subscribe(
      //   (data) => {
      //     // this.closeBtn.nativeElement.click();
      //     console.log('pool : ' , JSON.stringify(data));
      //     this.cleraAllfield();
      //     this.routeTo('/setting');
      //   },
      //   (err) => {
      //     console.log('err', err);
      //     this.logError(err);
      //   });
    } else {
      this.logError('Password Not Matching', 'info');
    }

  }

  cleraAllfield() {
    this.repassword = this.password = this.poolName =
      this.poolId = this.selectedeventId = this.selectedtypeId =
        this.selectedleugueId = '';
  }

  routeTo(pageroute: string, id) {
    this.routerext.navigate([pageroute], {
      queryParams: { id: id },
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }

  account() {
    localStorage.setItem('isLogin', 'yes');
    this.routeTo('/signup', 0);
  }
}
