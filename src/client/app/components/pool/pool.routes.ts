import { PoolComponent } from './pool.component';

export const PoolRoutes: Array<any> = [
  {
    path: 'pool',
    component: PoolComponent
  }
];
