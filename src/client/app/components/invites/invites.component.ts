// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { Pool, UserInvites} from '../../interfaces/pool';
import { SignUp } from '../../interfaces/user';

// services
import { PoolService } from '../../services/pool.services';
import { PoolEventService } from '../../services/poolevent.services';
import { PoolTypeService } from '../../services/pooltype.services';
import { LeagueService } from '../../services/league.services';
import { AcccountService } from '../../services/account.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'ss-invites',
  templateUrl: 'invites.component.html',
  styleUrls: ['../css/style.css','../css/res.css'],
  providers: [PoolService, PoolEventService, PoolTypeService,
    LeagueService, AcccountService]
})

export class InvitesComponent implements OnInit {
  max;
  user1 = '';
  user2 = '';
  user3 = '';
  user4 = '';
  user5 = '';
  user6 = '';
  user7 = '';
  user8 = '';
  user9 = '';
  user10 = '';
  user11 = '';
  user12 = '';
  user13 = '';
  user14 = '';
  user15 = '';
  user16 = '';
  user17 = '';
  user18 = '';
  user: SignUp;
  user_id = '';
  userName;
  poolName;
  options = '';
  sets = '';
  tempPool: Pool;
  pool : Pool;
  msgs: Message[] = [];
  userEmails: any[] = [];
  count = 18;
  url = 'http://localhost:5555/join?id=2';
  @ViewChild('one') d1: ElementRef;
  // @ViewChild('input') inputs;

  constructor(private poolService: PoolService,
              private poolEventService: PoolEventService,
              private poolTypeService: PoolTypeService,
              private leagueService: LeagueService,
              private acccountService: AcccountService
    , private route: ActivatedRoute
    , public routerext: RouterExtensions,
              private elementRef: ElementRef) {
  }

  ngOnInit() {

    if (!localStorage.getItem('currentUser') ||
      !localStorage.getItem('token')) {
      this.routeTo('/signin');
    }

    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.acccountService.getById(this.user._id).subscribe(user => {
          if (user) {
            console.log('user find : ', user.userName);
            this.user_id = user._id;
            this.userName = user.userName;
          } else {
            this.routeTo('/signin');
          }
        }
        , error => this.logError('User Not Found ' + error, 'error')
      );
    }

    if (localStorage.getItem('currentPool')) {
      this.tempPool = JSON.parse(localStorage.getItem('currentPool'));
      this.poolName = this.tempPool.name;
    }


  }

  invitesUser() {
    // console.log(this.inputs); //this will give access to id and values (explore it on google chromes console)
    // console.log(this.inputs.toArray().map(x => x.value)) // this gives access to values

    // console.log('count', this.count);
    // for(let i = 1 ; i <= this.count ; this.count++) {
    //   if(this.us)
    // }

    if(localStorage.getItem('currentPool')) {
      this.pool = JSON.parse(localStorage.getItem('currentPool'));
    }
    this.userEmails = [];
    if (this.user1) {
      this.userEmails.push(this.user1);
    }
    if (this.user2) {
      this.userEmails.push(this.user2);
    }
    if (this.user3) {
      this.userEmails.push(this.user3);
    }
    if (this.user4) {
      this.userEmails.push(this.user4);
    }
    if (this.user5) {
      this.userEmails.push(this.user5);
    }
    if (this.user6) {
      this.userEmails.push(this.user6);
    }
    if (this.user7) {
      this.userEmails.push(this.user7);
    }
    if (this.user8) {
      this.userEmails.push(this.user8);
    }
    if (this.user9) {
      this.userEmails.push(this.user9);
    }
    if (this.user10) {
      this.userEmails.push(this.user10);
    }
    if (this.user11) {
      this.userEmails.push(this.user11);
    }
    if (this.user12) {
      this.userEmails.push(this.user12);
    }
    if (this.user13) {
      this.userEmails.push(this.user13);
    }
    if (this.user14) {
      this.userEmails.push(this.user14);
    }
    if (this.user15) {
      this.userEmails.push(this.user15);
    }
    if (this.user16) {
      this.userEmails.push(this.user16);
    }
    if (this.user17) {
      this.userEmails.push(this.user17);
    }
    if (this.user18) {
      this.userEmails.push(this.user18);
    }

    let userInvites: UserInvites = {} as any;
    console.log('this.userEmails', this.userEmails);
    userInvites.users = this.userEmails;
    userInvites.route = this.url;
    userInvites.poolId = this.pool.id;
    userInvites.password = this.pool.password;
    this.poolService.invites(userInvites).subscribe(succes => {
        this.logError('Succesfully invitaions send ', 'succes');
        this.cleraAllfield();
       },
      err => {
        this.logError('Error on invites users ', 'error');
      });
  }

  addTextbox() {
    // let countB = 'user' + (this.count + 1);
    // this.count = this.count + 2;
    // let user1 = 'user' + this.count;
    // // this.d1.nativeElement.insertAdjacentHTML('beforeend', '<input type="text" [(ngModel)]="user1" name="user1"><input type="text" [(ngModel)]="user2" name="user2" class="input-1">');
    // var d1 = this.elementRef.nativeElement.querySelector('.one');
    // d1.insertAdjacentHTML('beforeend', '<input type="text" [(ngModel)]="countB" name="countB"><input type="text" [(ngModel)]="user1" name="user1" [ngStyle]="{margin-right:0px;}">');
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  cleraAllfield() {
    this.user1 = this.user2 = this.user3 = this.user4 =
      this.user5 = this.user6 = this.user7 = this.user8 =
        this.user9 = this.user10 = this.user11 = this.user12 =
          this.user13 = this.user14 = this.user15 =  this.user16 =
            this.user17 = this.user18 = '';
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
