// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';


@Component({
  moduleId: module.id,
  selector: 'ss-right',
  templateUrl: 'rightside.component.html',
  styleUrls: ['../css/style.css','../css/res.css']
})

export class RightSideComponent implements OnInit {

  constructor(private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    console.log('right');
  }

  logError(err: any) {
    console.log('error' , err);
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
