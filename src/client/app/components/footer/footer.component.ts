// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';


@Component({
  moduleId: module.id,
  selector: 'ss-footer',
  templateUrl: 'footer.component.html',
  styleUrls: ['../css/bootstrap.css','../css/style1.css']
})

export class FooterComponent implements OnInit {

  constructor(private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    console.log('footer');
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
