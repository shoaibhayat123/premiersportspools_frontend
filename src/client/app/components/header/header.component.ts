// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';
import { Location } from '@angular/common';

// interfaces
import { Pool, Count, PoolLeague, PoolEvent, PoolType } from '../../interfaces/pool';

// services
import { PoolService } from '../../services/pool.services';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Rx';


@Component({
  moduleId: module.id,
  selector: 'ss-header',
  templateUrl: 'header.component.html',
  styleUrls: ['../css/bootstrap.css','../css/style1.css'],
  providers: [PoolService]
})

export class HeaderComponent implements OnInit {
  msgs: Message[] = [];router; subscribe;

  constructor(private poolService: PoolService,
              private route: ActivatedRoute,
              private location: Location
    ,public routerext: RouterExtensions) {
    this.router = route;
  }

  ngOnInit() {
    console.log('header');
  }

  lastPool() {
    // console.log(location.pathname);
    if(location.pathname.toString() !== '/dash') {
      if (localStorage.getItem('currentUser')) {
        let user = JSON.parse(localStorage.getItem('currentUser'));
        console.log('user id' ,user._id);
        this.poolService.getLastPool(user._id).subscribe(pool => {
            pool.league_id = pool.league_id === null ? ' ': pool.league_id;
            pool.poolType_id = pool.poolType_id === null ? ' ': pool.poolType_id;
            pool.poolEvent_id = pool.poolEvent_id === null ? ' ': pool.poolEvent_id;
            localStorage.setItem('currentPool', JSON.stringify(pool));
              this.routeTo('/dash');
          },
        err => {
          this.logError('You donot have any own created pool, You can join another pool or create you own pool  ', 'info');
        });
      }
    } else {
      console.log('click id');
      this.logError('Already on your Pools...', 'info');
    }
  }

  reload() {
    this.subscribe.unsubscribe();
    location.reload();
  }

  noPool() {
    this.subscribe.unsubscribe();
    this.routeTo('/');
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
