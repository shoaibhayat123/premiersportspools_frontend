// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// interfaces
import { Pool } from '../../interfaces/pool';
import { SignUp } from '../../interfaces/user';

// services
import { PoolService } from '../../services/pool.services';
import { AcccountService } from '../../services/account.services';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'ss-roaster',
  templateUrl: 'roaster.component.html',
  styleUrls: ['../css/style.css','../css/res.css'],
  providers: [PoolService, AcccountService]
})

export class RoasterComponent implements OnInit {
  pool: Pool;
  user: SignUp; user_id = ''; userName; poolName;
  msgs: Message[] = [];
  cols: any[] = [];

  constructor(private poolService: PoolService,
              private acccountService: AcccountService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {}

  ngOnInit() {
    if (!localStorage.getItem('currentUser') ||
      !localStorage.getItem('token')) {
      this.routeTo('/signin');
    }

    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.acccountService.getById(this.user._id).subscribe(user => {
          if(user) {
            console.log('user find : ' , user.userName);
            this.user_id = user._id;
            this.userName = user.userName;
          } else {
            this.routeTo('/signin');
          }
        }
        , error => this.logError('User Not Found ' + error, 'error')
      );
    }

    if(localStorage.getItem('currentPool')) {
      this.pool = JSON.parse(localStorage.getItem('currentPool'));
      this.poolName = this.pool.name;
      this.poolService.getPoolMemBy(this.pool.id).subscribe(succes => {
        console.log('succes', succes.PoolMember);
        for(let i = 0; i < succes.PoolMember.length; i++) {
          let pooldata = JSON.stringify(succes.PoolMember[i]);
          console.log('pooldata', pooldata);

          this.cols.push({data : succes.PoolMember[i], poolEntry: this.pool.maxEntries});
        }

        },
      err => {
        this.logError('Error! On loading pool members ' + err, 'error');
      });
    }
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
