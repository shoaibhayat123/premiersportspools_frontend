// core libs
import { Component, OnInit , ElementRef, ViewChild} from '@angular/core';

// interfaces
import { Pool, Count, PoolMemReq, PoolScore, PoolType } from '../../interfaces/pool';
import { SignUp } from '../../interfaces/user';

// services
import { PoolService } from '../../services/pool.services';
import { PoolEventService } from '../../services/poolevent.services';
import { PoolTypeService } from '../../services/pooltype.services';
import { LeagueService } from '../../services/league.services';
import { AcccountService } from '../../services/account.services';

// routes libs
import { ActivatedRoute} from '@angular/router';
import { RouterExtensions, Config } from '../../modules/core/index';

// UI libs
import {Message, SelectItem } from 'primeng/primeng';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Rx';

// import '../../../node_modules/jquery/dist/jquery.js';
// import '../../../node_modules/bootstrap/dist/js/bootstrap.min.js';

// import '../../../src/client/assets/assets/js/jquery-3.3.1.min.js';
// import '../../../src/client/assets/assets/js/bootstrap.min.js';


@Component({
  moduleId: module.id,
  selector: 'ss-chosen',
  templateUrl: 'chosenDashboard.component.html',
  styleUrls: ['../css/bootstrap.css','../css/style1.css'],
  providers: [PoolService, PoolEventService, PoolTypeService,
    LeagueService, AcccountService]
})

export class ChosenDashboardComponent implements OnInit {

  user: SignUp; user_id = ''; userName; poolName; options = ''; sets = '';
  tempPool: Pool; shortName; shrtName = ''; short = false; fullName = '';
  msgs: Message[] = []; item = []; displayName= ''; joinPools; createPools;
  pool_id;

  constructor(private poolService: PoolService,
              private poolEventService: PoolEventService,
              private poolTypeService: PoolTypeService,
              private leagueService: LeagueService,
              private acccountService: AcccountService
    ,private route: ActivatedRoute
    ,public routerext: RouterExtensions) {

    for(let i = 1; i <= 100; i++) {
      this.item[i] = ' ';
    }
  }

  ngOnInit() {
    if (!localStorage.getItem('currentUser') ||
      !localStorage.getItem('token')) {
      this.routeTo('/signin');
    }

    if (localStorage.getItem('currentUser')) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.user._id);
      this.acccountService.getById(this.user._id).subscribe(user => {
          if(user) {
            console.log('pick user find : ' , user.userName);
            this.user_id = user._id;
            this.userName = user.userName;
            if(localStorage.getItem('currentPool')) {
              this.tempPool = JSON.parse(localStorage.getItem('currentPool'));
              console.log('pool Id' , this.tempPool.id);
              this.poolService.getBy(this.tempPool.id).subscribe(data => {
                if (data) {
                  this.pool_id = data._id;
                  console.log('find data poll', data);
                  this.poolService.getByUserId(user._id).subscribe(pools => {
                    if (pools.joinPools) {
                      this.joinPools = pools.joinPools;
                      console.log('this.joinPools', this.joinPools);
                    }
                    if (pools.createPools) {
                      this.createPools = pools.createPools;
                      console.log('this.createPools', this.createPools);
                    }
                  }, err => {
                    this.logError('Error! Getting Pools ' + err, 'error');
                  });

                  // pick data
                  let poolMemReq: PoolMemReq = {} as any;
                  poolMemReq.pool_id = this.tempPool.id;
                  poolMemReq.user_id = this.user_id;
                  this.poolService.getPoolMemByUP(poolMemReq).subscribe(mem => {
                      if (mem.shortName !== '') {
                        console.log('mem' + mem.shortName);
                        this.shortName = this.shrtName = mem.shortName;
                        this.short = true;
                      }
                      this.displayName = mem.displayName;
                      let poolScore: PoolScore = {} as any;
                      poolScore.pool_id = this.tempPool.id;
                      poolScore.quater = '1';
                      this.poolService.getScoreByPQ(poolScore).subscribe(scoreCard => {
                          console.log('scoreArr', scoreCard.scoreArr.length);
                          for (let i = 0; i < scoreCard.scoreArr.length; i++) {
                            let scoreArr = scoreCard.scoreArr[i];
                            console.log('scoreArr', scoreArr);
                            var array = scoreArr.split(',');
                            console.log(array[0] + ',' + array[1] + ',' + array[2]);
                            this.item[array[2]] = array[1];
                          }
                        },
                        err => {
                          this.logError('Error in load board ' + err, 'error');
                        });
                    },
                    err => {
                      console.log('err', err);
                    });
                } else {
                  this.lastPool();
                }
              });
            } else {
              this.lastPool();
            }
          } else {
            this.routeTo('/signin');
          }
        }
        , error => this.logError('User Not Found ' + error, 'error')
      );
    }
  }

  lastPool() {
    if (localStorage.getItem('currentUser')) {
      let user = JSON.parse(localStorage.getItem('currentUser'));
      console.log(user._id);
      this.poolService.getLastPool(user._id).subscribe(pool => {
          // if(pool) {
          pool.league_id = pool.league_id === null ? ' ': pool.league_id;
          pool.poolType_id = pool.poolType_id === null ? ' ': pool.poolType_id;
          pool.poolEvent_id = pool.poolEvent_id === null ? ' ': pool.poolEvent_id;
          localStorage.setItem('currentPool', JSON.stringify(pool));
          this.routeTo('/dash');
          // } else {
          //   this.logError('You donot have any own created pool, You can join another pool or create you own pool ', 'succes');
          //   let timer = Observable.timer(2000, 1000);
          //   this.subscribe = timer.subscribe(t => {
          //     console.log('loading...');
          //     this.noPool();
          //   });
          //
          // }
        },
        err => {
          this.logError('You donot have any own created pool, You can join another pool or create you own pool ', 'info');

          // let timer = Observable.timer(2000, 1000);
          // this.subscribe = timer.subscribe(t => {
          //   console.log('loading...');
          //   this.noPool();
          // });
          // this.routeTo('/');
        });
    }
  }

  goToPool(id: any) {
    console.log('pool id' , id);
    if(this.pool_id !== id) {
      this.poolService.getById(id).subscribe(pool => {
          // console.log('pool pool id', pool.poolType_id._id);
          pool.league_id = pool.league_id === null ? ' ' : pool.league_id;
          pool.poolType_id = pool.poolType_id === null ? ' ' : pool.poolType_id;
          pool.poolEvent_id = pool.poolEvent_id === null ? ' ' : pool.poolEvent_id;
          localStorage.setItem('currentPool', JSON.stringify(pool));
          // this.fillform(pool);
          // this.routeTo('/dash');
          // this.logError('Loading Pool ' + pool.id + ' Dashboard.... ', 'succes');
          // let timer = Observable.timer(2000,1000);
          // timer.subscribe(t=> {
          //   console.log('loading...');
          //   location.reload();
          // });
          console.log('loading...');
          // location.reload();
          this.routeTo('/dash');
          // ChangeDetectorRef.markForCheck();
        },
        err => {
          this.logError('Error! Move on current pool ' + err, 'error');
        });
    } else {
      this.logError('You are already on this pool ', 'info');
    }
  }

  onSearchChange(val: any) {
    if(this.shrtName === '' || this.shrtName === null) {
      console.log('val in update', val);
      let poolMemReq: PoolMemReq = {} as any;
      poolMemReq.pool_id = this.tempPool.id;
      poolMemReq.user_id = this.user_id;
      poolMemReq.shortName = val;
      this.poolService.getPoolMemByUPAndUpdate(poolMemReq).subscribe(mem => {
          console.log('member Update', mem);
          this.shortName = this.shrtName = mem.shortName;

        },
        err => {
          this.logError('Error in set the short name', 'error');
        });
    } else {
      this.shortName = this.shrtName;
      this.logError('Abbrivation Already Set OR You Cannot Change More', 'info');
    }
  }

  onDisSearchChange(val: any) {
    // if(this.shrtName === '') {
    console.log('val in update', val);
    let poolMemReq: PoolMemReq = {} as any;
    poolMemReq.pool_id = this.tempPool.id;
    poolMemReq.user_id = this.user_id;
    poolMemReq.displayName = val;
    this.poolService.getPoolMemByUPDisAndUpdate(poolMemReq).subscribe(mem => {
        console.log('member Update', mem);
        this.displayName = mem.displayName;
      },
      err => {
        this.logError('Error in set the short name', 'error');
      });
    // } else {
    //   this.shortName = this.shrtName;
    //   this.logError('Abbrivation Already Set OR You Cannot Change More', 'info');
    // }
  }


  getVal(val: any) {
    if(this.shrtName !== '') {
      // console.log('val', val.originalTarget.attributes.index.value);
      console.log('val', val.attributes.index.value);
      let poolScore: PoolScore = {} as any;
      poolScore.pool_id = this.tempPool.id;
      poolScore.user_id = this.user_id;
      poolScore.quater = '1';
      poolScore.index = val.attributes.index.value;
      this.poolService.pickUp(poolScore).subscribe(scoreCard => {
        for(let i = 0; i < scoreCard.scoreArr.length; i++) {
          let scoreArr = scoreCard.scoreArr[i];
          console.log('scoreArr', scoreArr);
          var array = scoreArr.split(',');
          console.log(array[0] + ',' + array[1] + ',' + array[2]);
          this.item[array[2]] = array[1];
        }
        // val.innerHTML = this.shrtName;
      }, err => {
        this.logError('No more picks ' + err, 'error');
      });
    } else {
      this.logError('Please Set Abbrivation', 'info');
    }
  }

  logError(err: any, type: any) {
    this.msgs = [];
    this.msgs.push({
      severity: type,
      detail: err
    });
  }

  cleraAllfield() {
    // this.max = '';
  }

  routeTo(pageroute: string) {
    this.routerext.navigate([pageroute], {
      transition: {
        duration: 1000,
        name: 'slideTop',
      }
    });
  }
}
