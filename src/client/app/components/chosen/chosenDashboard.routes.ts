import { ChosenDashboardComponent } from './chosenDashboard.component';

export const ChosenDashboardRoutes: Array<any> = [
  {
    path: 'chosen',
    component: ChosenDashboardComponent
  }
];
