import { ChosenComponent } from './chosen.component';

export const ChosenRoutes: Array<any> = [
  {
    path: 'cchosen',
    component: ChosenComponent
  }
];
