// core libs
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';

// interfaces
import { PoolType } from '../interfaces/pool';

// Global Constant
import { API_STARTPOINT} from '../components/global.constant';

// UI libs
import {MenuItem, Message } from 'primeng/primeng';

@Injectable()
export class PoolTypeService {
  msgs: Message[] = [];

  constructor(private http: Http) {}

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  submitPoolType(poolType: PoolType) {
    return this.http.post(API_STARTPOINT + '/type', poolType).map((res) => { res.json(); });
  }

  //----------------------- Edit or Update API-------------------------------//

  updateBy(search:string, poolType: PoolType) {
    console.log('user', JSON.stringify(poolType));
    return this.http.put(API_STARTPOINT + '/type/' + search, JSON.stringify(poolType
    )).map((res) => res.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(search:string) {
    console.log('deleted',search);
    return this.http.delete(API_STARTPOINT + '/type/del/' + search)
      .map((response) => { response.json();
        console.log('response' , response.json()); } );
  }

  //----------------------- Delete All API-------------------------------//

  // deleteAll() {
  //   return this.http.delete(API_STARTPOINT + '/user')
  //     .map((response) => { response.json();
  //       console.log('response' , response.json()); } );
  // }

  //----------------------- Read or Read All API-------------------------------//

  getPoolTypes() {
    return this.http.get(API_STARTPOINT + '/types')
      .map(response => response.json());
  }

  getBy(search:string) {
    return this.http.get(API_STARTPOINT + '/type/' + search)
      .map(response => response.json());
  }

  getById(id:string) {
    return this.http.get(API_STARTPOINT + '/type/id/' + id)
      .map(response => response.json());
  }
  //
  // getByUserType(userTypeID:string) {
  //   return this.http.get(API_STARTPOINT + '/users/userTypeID/' + userTypeID)
  //     .map(response => response.json());
  // }
}
