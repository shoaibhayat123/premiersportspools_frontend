// core libs
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';

// interfaces
import { PoolLeague } from '../interfaces/pool';

// Global Constant
import { API_STARTPOINT} from '../components/global.constant';

// UI libs
import {MenuItem, Message } from 'primeng/primeng';

@Injectable()
export class LeagueService {
  msgs: Message[] = [];

  constructor(private http: Http) {}

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  // submitPoolType(poolEvent: PoolEvent) {
  //   return this.http.post(API_STARTPOINT + '/type', poolEvent).map((res) => { res.json(); });
  // }

  //----------------------- Edit or Update API-------------------------------//

  // updateBy(search:string, poolEvent: PoolEvent) {
  //   console.log('user', JSON.stringify(poolEvent));
  //   return this.http.put(API_STARTPOINT + '/type/' + search, JSON.stringify(poolEvent
  //   )).map((res) => res.json());
  // }

  //----------------------- Delete or Delete By API-------------------------------//

  // deleteBy(search:string) {
  //   console.log('deleted',search);
  //   return this.http.delete(API_STARTPOINT + '/type/del/' + search)
  //     .map((response) => { response.json();
  //       console.log('response' , response.json()); } );
  // }
  //
  // deleteById(search:string) {
  //   console.log('deleted',search);
  //   return this.http.delete(API_STARTPOINT + '/type/del/id/' + search)
  //     .map((response) => { response.json();
  //       console.log('response' , response.json()); } );
  // }

  //----------------------- Delete All API-------------------------------//

  // deleteAll() {
  //   return this.http.delete(API_STARTPOINT + '/user')
  //     .map((response) => { response.json();
  //       console.log('response' , response.json()); } );
  // }

  //----------------------- Read or Read All API-------------------------------//

  getLeagues() {
    return this.http.get(API_STARTPOINT + '/sports')
      .map(response => response.json());
  }

  getBy(search:string) {
    return this.http.get(API_STARTPOINT + '/sport/' + search)
      .map(response => response.json());
  }

  getById(id:string) {
    return this.http.get(API_STARTPOINT + '/sport/id/' + id)
      .map(response => response.json());
  }

  // getByUserType(userTypeID:string) {
  //   return this.http.get(API_STARTPOINT + '/users/userTypeID/' + userTypeID)
  //     .map(response => response.json());
  // }
}
