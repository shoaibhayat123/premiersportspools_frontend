// core libs
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';

// interfaces
import { Pool, LoginPool, UserInvites, PoolMemReq, PoolScore, PoolCreater } from '../interfaces/pool';

// Global Constant
import { API_STARTPOINT} from '../components/global.constant';

// UI libs
import {MenuItem, Message } from 'primeng/primeng';

@Injectable()
export class PoolService {
  msgs: Message[] = [];

  constructor(private http: Http) {
  }

  //-----------------------Invites Users------------------------------------

  invites(users: UserInvites) {
    return this.http.post(API_STARTPOINT + '/invites', users).map((res) => res.json());
  }

  //-----------------------Score Users------------------------------------

  pickUp(poolScore: PoolScore) {
    return this.http.post(API_STARTPOINT + '/score', poolScore).map((res) => res.json());
  }

  //-----------------------Login Pool------------------------------------

  signIn(pool: LoginPool) {
    return this.http.post(API_STARTPOINT + '/plogin', pool).map((res) => res.json());
  }

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  getCount() {
    return this.http.get(API_STARTPOINT + '/count').map(response => response.json());
  }

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  submitPool(pool: Pool) {
    return this.http.post(API_STARTPOINT + '/pool', pool).map((res) => {
      console.log(res.json());
    });
  }

  //----------------------- Edit or Update API-------------------------------//

  // updateBy(search:string, pool: Pool) {
  //   console.log('user', JSON.stringify(pool));
  //   return this.http.put(API_STARTPOINT + '/pool/' + search, JSON.stringify(pool
  //   )).map((res) => res.json());
  // }

  //----------------------- Delete or Delete By API-------------------------------//

  deleteBy(search: string) {
    console.log('deleted', search);
    return this.http.delete(API_STARTPOINT + '/pool/del/' + search)
      .map((response) => {
        response.json();
        console.log('response', response.json());
      });
  }

  //----------------------- Delete All API-------------------------------//

  // deleteAll() {
  //   return this.http.delete(API_STARTPOINT + '/user')
  //     .map((response) => { response.json();
  //       console.log('response' , response.json()); } );
  // }

  //----------------------- Read or Read All API-------------------------------//

  getPool() {
    return this.http.get(API_STARTPOINT + '/pools')
      .map(response => response.json());
  }

  getScoreByPQ(search: PoolScore) {
    return this.http.post(API_STARTPOINT + '/get_score', search)
      .map(response => response.json());
  }

  getPoolMemBy(search: string) {
    return this.http.get(API_STARTPOINT + '/roaster/' + search)
      .map(response => response.json());
  }

  getPoolMemByUP(search: PoolMemReq) {
    return this.http.post(API_STARTPOINT + '/get_member', search)
      .map(response => response.json());
  }

  getPoolMemByUPAndUpdate(search: PoolMemReq) {
    return this.http.post(API_STARTPOINT + '/member', search)
      .map(response => response.json());
  }

  getPoolMemByUPDisAndUpdate(search: PoolMemReq) {
    return this.http.post(API_STARTPOINT + '/member_d', search)
      .map(response => response.json());
  }

  getPoolMemByUserId(search: string) {
    return this.http.get(API_STARTPOINT + '/roaster/id/' + search)
      .map(response => response.json());
  }

  getBy(search: string) {
    return this.http.get(API_STARTPOINT + '/pool/' + search)
      .map(response => response.json());
  }

  getById(search: string) {
    return this.http.get(API_STARTPOINT + '/pool/id/' + search)
      .map(response => response.json());
  }

  getByUserId(search: string) {
    return this.http.get(API_STARTPOINT + '/get_pool/' + search)
      .map(response => response.json());
  }

  getLastPool(search: string) {
    return this.http.get(API_STARTPOINT + '/pool/userId/last/' + search)
      .map(response => response.json());
  }

  getPoolByPC(search: PoolCreater) {
    return this.http.post(API_STARTPOINT + '/iscompool', search)
      .map(response => response.json());
  }

  // getByUserType(userTypeID:string) {
  //   return this.http.get(API_STARTPOINT + '/users/userTypeID/' + userTypeID)
  //     .map(response => response.json());
  // }
}
