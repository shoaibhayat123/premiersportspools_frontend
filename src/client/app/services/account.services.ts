// core libs
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';

// interfaces
import { SignUp, SignIn } from '../interfaces/user';
import { API_STARTPOINT} from '../components/global.constant';

// UI libs
import {MenuItem, Message } from 'primeng/primeng';

@Injectable()
export class AcccountService {
  msgs: Message[] = [];

  constructor(private http: Http) {}

  //----------------------- Create or Insert API (Sign>Up)------------------------------//

  signUp(user: SignUp) {
    return this.http.post(API_STARTPOINT + '/signup', user).map((res) => { res.json(); });
  }

  //----------------------- Read or Login API-------------------------------//

  signIn(user: SignIn) {
    return this.http.post(API_STARTPOINT + '/login', user).map((res) => res.json());
  }

  //----------------------- Edit or Update API-------------------------------//

  updateUser(any:string, user:SignUp) {
    console.log('user', JSON.stringify(user));
    return this.http.put(API_STARTPOINT + '/user/' + any, user).map((res) => res.json());
  }

  //----------------------- Delete or Delete By API-------------------------------//

  // deleteBy(email:string) {
  //   console.log('deleted',email);
  //   return this.http.delete(API_STARTPOINT + '/user/email/' + email)
  //     .map((response) => { response.json();
  //       console.log('response' , response.json()); } );
  // }

  //----------------------- Delete All API-------------------------------//

  // deleteAll() {
  //   return this.http.delete(API_STARTPOINT + '/user')
  //     .map((response) => { response.json();
  //       console.log('response' , response.json()); } );
  // }

  //----------------------- Read or Read All API-------------------------------//

  getUsers() {
    return this.http.get(API_STARTPOINT + '/users')
      .map(response => response.json());
  }

  getBy(any:string) {
    return this.http.get(API_STARTPOINT + '/user/' + any)
      .map(response => response.json());
  }

  getById(id:string) {
    return this.http.get(API_STARTPOINT + '/user/id/' + id)
      .map(response => response.json());
  }

  // getByUserType(userTypeID:string) {
  //   return this.http.get(API_STARTPOINT + '/users/userTypeID/' + userTypeID)
  //     .map(response => response.json());
  // }
}
