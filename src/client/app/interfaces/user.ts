export interface SignUp {
  _id: string;
  id: string;
  fullName: string;
  userName: string;
  email: string;
  password: string;
  createdDate: string;
  createdBy: string;
  modifiedDate: string;
  phoneNumber: string;
  isActive:boolean;
  isDelete:boolean;
  isAdmin:boolean;
}

export interface SignIn {
  email: string;
  userName: string;
  password: string;
}
