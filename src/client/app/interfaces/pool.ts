export interface PoolType {
  _id: string;
  id: string;
  name: string;
  createdDate: string;
  createdBy: string;
  isActive: boolean;
  isDelete: boolean;
}

export interface PoolEvent {
  _id: string;
  id: string;
  name: string;
  startDate: string;
  endDate: string;
  days: any[];
  createdDate: string;
  createdBy: string;
  isActive: boolean;
  isDelete: boolean;
}

export interface PoolLeague {
  _id: string;
  id: string;
  name: string;
  startDate: string;
  endDate: string;
  days: any[];
  createdDate: string;
  createdBy: string;
  isActive: boolean;
  isDelete: boolean;
}

export interface Pool {
  _id: string;
  id: string;
  name: string;
  password: string;
  createdDate: string;
  createdBy: string;
  modifiedDate: string;
  members: any[];
  isActive:boolean;
  isDelete:boolean;
  poolEvent_id: string;
  poolType_id: string;
  league_id: string;
  winnerFreq: string;
  numSets: string;
  maxEntries: string;
}

export interface LoginPool {
  id: string;
  password: string;
  user_id: string;
}

export interface Count {
  _id: string;
  id: string;
}

export interface UserInvites {
  users: any[];
  route: string;
  poolId: string;
  password: string;
}

export interface PoolMemReq {
  pool_id : string;
  user_id : string;
  shortName : string;
  displayName : string;
}

export interface PoolScore {
  pool_id : string;
  user_id : string;
  shortName : string;
  quater: string;
  index: string;
}

export interface PoolCreater {
  id : string;
  createdBy : string;
}
